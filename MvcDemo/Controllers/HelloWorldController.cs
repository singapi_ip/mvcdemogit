﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcDemo.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: HelloWorld
        public ActionResult Index()
        {
            return View();
        }

        // GET: Helloword/Welcome
        public ActionResult Welcome(string name, int numTimes)
        {
            ViewBag.Message = "Hello " + name;
            ViewBag.numTimes = numTimes;

            //return HttpUtility.HtmlEncode(string.Format("Hello {0}, you already visit this website for: {1}", name, numTimes));
            return View();
        }
    }
}