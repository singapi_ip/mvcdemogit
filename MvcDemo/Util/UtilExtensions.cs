﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace MvcDemo.Util
{
    public static class UtilExtensions
    {
        public static void AddIfNotExists<T>(this DbSet<T> dbSet, T entity, Expression<Func<T, bool>> predicate)
            where T:class, new()
        {
            bool exists = dbSet.Any(predicate);
            if (!exists) { dbSet.Add(entity); }
        }
    }
}